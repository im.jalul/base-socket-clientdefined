
require('dotenv').config()

const runningPort = process.env.PORT || 8823

const SocketServer = require(`./socket/server`)
const socketEventHandle = require(`./socket/event-handle`)

const server = SocketServer.init(runningPort)
server.on(`connect`, socketEventHandle)