
/** Were storing connected clients here */
const Client = require(`./client-model`)
const clients = []

module.exports = {
  /**
   * To retrieve all connected clients.
   *
   * @returns {Client set}
   */
  getAll() {
    return clients
  },

  /**
   * To retrieve a connected client with user_id.
   *
   * @param {string} user_id User identifier
   *
   * @returns {Client Object}
   */
  get(user_id) {
    return clients.find(i => i.id === user_id)
  },


  /**
   * To retrieve a connected client with connection id.
   *
   * @param {string} conn_id Connection id that given from socket.io
   *
   * @returns {Client Object}
   */
  getByConnection(conn_id) {
    return clients.find(i => i.connections.includes(conn_id))
  },


  /**
   * Binding connection id with user id and added to list.
   *
   * @param {string} user_id User identifier
   * @param {string} conn_id Connection id that given from socket.io
   *
   * @returns {Client Object}
   */
  add(user_id, conn_id) {
    const client = this.get(user_id)

    if (client) return client.addConnection(conn_id)

    clients.push(new Client(user_id, conn_id))
  },


  /**
   * Remove a connected client from list.
   *
   * @param {string} user_id User identifier
   *
   * @returns {Client Object}
   */
  delete(user_id) {
    const index = clients.findIndex(i => i.id == user_id)
    if (index >= 0) clients.splice(index, 1)
  },
}