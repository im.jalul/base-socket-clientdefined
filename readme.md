
1. Running socket server

> node src/app.js

2. Open sample page, `sample/connect-socket.html`

3. Try to use different id by adding query string, example `?id=this-is-your-id-or-token`

4. See result on server log.