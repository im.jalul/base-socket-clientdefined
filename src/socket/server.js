
exports.init = (port) => {
  const server = require('http').createServer();
  const socket = require(`socket.io`)(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
  });

  server.listen(port);
  console.log(`Socket running on port ${port}`);

  return socket;
}
