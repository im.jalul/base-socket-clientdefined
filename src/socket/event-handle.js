
const Clients = require(`./connected-clients`)
const LOGOUT_DELAY_IN_MS = process.env.LOGOUT_DELAY_IN_MS || 5000

/**
 *
 * @param {Object} socket contain id that define connection id from each client
 */
module.exports = async (socket) => {
  socket.on(`define`, (userid) => {
    console.log(`[Connect] Define ${socket.id} as ${userid}`)

    Clients.add(userid, socket.id)
  })

  socket.on(`disconnect`, () => {
    const client = Clients.getByConnection(socket.id)

    console.log(`[Disconnect] Connection ${client.id} disconnected`)
    client.revokeConnection(socket.id)

    // Try to trigger logout after 5s
    // if client did not reconnect, then logout executed.
    client.triggerLogout(LOGOUT_DELAY_IN_MS, () => Clients.delete(client.id))
  })

  socket.on(`log-connected-clients`, () => {
    console.log(Clients.getAll())
  })
}