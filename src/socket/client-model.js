
const LogoutHandle = require(`../helper/logout`)

class Client {
  constructor(user_id, conn_id) {
    this.id = user_id
    this.connections = []

    this.logoutBuffer = null

    if (conn_id) this.addConnection(conn_id)
  }


  /**
   * Adding a connection with `conn_id` to client connections.
   * this method will reset triggerLogout.
   *
   * @param {string} conn_id String that given from socket.io.
   */

  addConnection(conn_id) {
    this.connections.push(conn_id)

    // Disable trigger logout
    clearTimeout(this.logoutBuffer)
    this.logoutBuffer = false
  }


  /**
   * Remove connection with `conn_id` from client connections.
   *
   * @param {string} conn_id String that given from socket.io.
   */
  revokeConnection(conn_id) {
    const index = this.connections.indexOf(conn_id)

    if (index >= 0) this.connections.splice(index, 1)
  }


  /**
   * Trigger logout when client has no more connections.
   *
   * @param {int} delay Delay time to give time for reconnecting in ms.
   * @param {function} callback  will triggered when logout process is done.
   */
  async triggerLogout(delay, callback) {
    if (this.connections.length) return false

    this.logoutBuffer = setTimeout(() => {

      // Re-Checking if there is another connection, then do nothing.
      if (this.connections.length) return false

      return LogoutHandle(this.id).then(callback)
    }, delay)
  }
}

module.exports = Client
