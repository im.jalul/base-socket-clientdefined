
const axios = require(`axios`)

module.exports = async function(user_id) {

  /** Your logout logic here. */
  const token = user_id
  const logoutEndpoint = process.env.LOGOUT_ENDPOINT
  console.log(logoutEndpoint)

  const requestParams = {
    url: logoutEndpoint,
    method: `post`,
    headers: {
      ContentType: `application/json`,
      Authorization: `Bearer ${token}`
    },
    responseType: `json`,
  }

  return axios(requestParams)
    .then(response => response.data)
    .catch(err => console.log(`Failed to logging out`, err.message) || null)
}